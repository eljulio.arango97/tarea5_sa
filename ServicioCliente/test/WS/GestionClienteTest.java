/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import WS.GestionCliente;
import WS.GestionCliente;
import WS.GestionCliente;
import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author eljul
 */
public class GestionClienteTest {
    
    public GestionClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ObtenerZonaUsuario method, of class GestionCliente.
     */
    @Test
    public void testObtenerZonaUsuario() {
        System.out.println("Nombre " + this.getClass().getName());
        System.out.println("ObtenerZonaUsuario");
        String nombre = "Julian25";
        GestionCliente instance = new GestionCliente();
        int expResult = 3;
        int result = instance.ObtenerZonaUsuario(nombre);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(expResult != result){
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of IngresoRegistroCliente method, of class GestionCliente.
     */
    @Test
    public void testIngresoRegistroCliente() {
        System.out.println("IngresoRegistroCliente");
        String nombreUsuario = "Maxito15";
        int zonaActual = 7;
        GestionCliente instance = new GestionCliente();
        Boolean expResult = true;
        Boolean result = instance.IngresoRegistroCliente(nombreUsuario, zonaActual);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if(!Objects.equals(expResult, result)){
            fail("The test case is a prototype.");
        }
    }
    
}

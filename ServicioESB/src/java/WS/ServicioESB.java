/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import References.*;
/**
 *
 * @author eljul
 */
@javax.jws.WebService
public class ServicioESB {
    public String PeticionESB(String nombre) {
        ServicioClienteService servCliente = new ServicioClienteService();
        ServicioCliente portCliente = servCliente.getServicioClientePort();
        int zona = portCliente.peticionIni(nombre);
        
        ServicioRastreoService servRastreo = new ServicioRastreoService();
        ServicioRastreo portRastreo = servRastreo.getServicioRastreoPort();
        String placa = portRastreo.obtenerPropuestaPiloto(zona);
        
        ServicioPilotoService servPiloto = new ServicioPilotoService();
        ServicioPiloto portPiloto = servPiloto.getServicioPilotoPort();
        return "Estimado " + nombre + " los datos de su viaje son: " + 
                portPiloto.obtenerConductoresDisponibles(placa);
    }
}
